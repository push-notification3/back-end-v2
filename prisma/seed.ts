import { PrismaClient } from "@prisma/client";
import * as argon from "argon2";

const prisma = new PrismaClient();
async function main() {
  const pwHash = await argon.hash("123456");

  await prisma.user.createMany({
    data: [
      {
        username: "user1",
        password: pwHash,
      },
      {
        username: "user2",
        password: pwHash,
      },
      {
        username: "user3",
        password: pwHash,
      },
    ],
    skipDuplicates: true,
  });

  await prisma.firebaseTopic.createMany({
    data: [
      {
        key: "topic1",
        name: "Topic 1",
      },
      {
        key: "topic2",
        name: "Topic 2",
      },
      {
        key: "topic3",
        name: "Topic 3",
      },
    ],
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
