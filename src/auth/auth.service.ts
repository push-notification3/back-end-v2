import { ForbiddenException, UnauthorizedException, Injectable } from "@nestjs/common";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { PrismaService } from "src/prisma/prisma.service";
import { AuthDto } from "./dto";
import * as argon from "argon2";
import { JwtService } from "@nestjs/jwt";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService, private jwt: JwtService, private config: ConfigService) {}

  async signin(data: AuthDto) {
    const user = await this.prisma.user.findUnique({
      where: {
        username: data.username,
      },
    });
    if (!user) {
      throw new ForbiddenException("Tên tài khoản hoặc mật khẩu không đúng");
    }

    const valid = await argon.verify(user.password, data.password);
    if (!valid) {
      throw new ForbiddenException("Tên tài khoản hoặc mật khẩu không đúng");
    }

    const tokens = await this.signToken({
      userId: user.id,
      username: user.username,
    });

    return tokens;
  }

  async signToken(data: { userId: number; username: string }) {
    const payload = {
      sub: data.userId,
      username: data.username,
    };

    const secret = this.config.get("JWT_SECRET");
    const refreshSecret = this.config.get("JWT_REFRESH_SECRET");

    const accessToken = await this.jwt.signAsync(payload, {
      // expiresIn: "2h",
      expiresIn: "7d",
      secret: secret,
    });
    const refreshToken = await this.jwt.signAsync(payload, {
      // expiresIn: "7d",
      secret: refreshSecret,
    });

    const accessTokenExpiresIn = this.jwt.decode(accessToken) as { exp: number };

    return { accessToken, refreshToken, exp: accessTokenExpiresIn.exp };
  }

  async getMe(userId: number) {
    try {
      const user = await this.prisma.user.findUniqueOrThrow({
        where: {
          id: userId,
        },
        select: {
          id: true,
          username: true,
          createdAt: true,
        },
      });

      return user;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === "P2025") {
          throw new UnauthorizedException("Tài khoản không tồn tại");
        }
      }
      throw error;
    }
  }
}
