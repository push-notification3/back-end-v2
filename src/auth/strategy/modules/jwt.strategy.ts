import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";

export type JwtPayload = {
  sub: number; // userId
  username: string;
  iat: number;
  exp: number;
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt") {
  constructor(config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: config.get("JWT_SECRET"),
    });
  }
  async validate(payload: JwtPayload) {
    // console.log(payload);
    // const user = await this.prisma.user.findUnique({
    //   where: {
    //     id: payload.sub,
    //   },
    // });
    // if (!user) {
    //   return null;
    // }
    // delete user.password;
    // return user;
    return payload;
  }
}
