import { Body, Controller, Get, Post, HttpCode, HttpStatus, UseGuards } from "@nestjs/common";
import { ApiOkResponse, ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { AuthService } from "./auth.service";
import { AuthDto } from "./dto";
import { GetUser } from "../auth/decorator";
import { JwtGuard } from "../auth/guard";
import type { JwtPayload } from "./strategy";

@Controller({
  path: "auth",
  version: "1",
})
@ApiTags("Auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse({
    description: "Đăng nhập",
    type: AuthDto,
  })
  @HttpCode(HttpStatus.OK)
  @Post("signin")
  signin(@Body() dto: AuthDto) {
    return this.authService.signin(dto);
  }

  @Get("me")
  @ApiBearerAuth()
  @UseGuards(JwtGuard)
  getMe(@GetUser() user: JwtPayload) {
    return this.authService.getMe(user.sub);
  }
}
