import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "class-validator";
import { trim } from "src/common/helper";

export class AuthDto {
  @ApiProperty({
    required: true,
    description: "username",
  })
  @Transform(({ value }) => trim(value))
  @IsString()
  @IsNotEmpty({ message: "Tên tài khoản không được để trống" })
  username: string;

  @ApiProperty({
    required: true,
    description: "Mật khẩu",
  })
  @IsString()
  @IsNotEmpty({ message: "Mật khẩu là bắt buộc" })
  password: string;
}
