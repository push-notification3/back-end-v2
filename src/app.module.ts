import { ConfigModule } from "@nestjs/config";

import { Module } from "@nestjs/common";
import { PrismaModule } from "./prisma/prisma.module";
import { BaseModule } from "./base/base.module";
import { AuthModule } from "./auth/auth.module";
import { FirebaseModule } from "nestjs-firebase";
import googleApplicationCredentialFile from "./assets/firebase-admin-sdk-key";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AuthModule,
    BaseModule,
    FirebaseModule.forRoot({
      googleApplicationCredential: {
        projectId: googleApplicationCredentialFile.project_id,
        clientEmail: googleApplicationCredentialFile.client_email,
        privateKey: googleApplicationCredentialFile.private_key,
      },
    }),
    PrismaModule,
  ],
})
export class AppModule {}
