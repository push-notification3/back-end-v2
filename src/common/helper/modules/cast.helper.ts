interface ToNumberOptions {
  default?: number;
  min?: number;
  max?: number;
}

export function toLowerCase(value: string): string | undefined {
  if (value === undefined) {
    return undefined;
  }
  return value.toLowerCase();
}

export function trim(value: string): string | undefined {
  if (value === undefined) {
    return undefined;
  }
  return value.trim();
}

export function toDate(value: string): Date | undefined {
  if (value === undefined) {
    return undefined;
  }
  return new Date(value);
}

export function toBoolean(value: string): boolean | undefined {
  if (value === undefined) {
    return undefined;
  }
  value = value.toLowerCase();
  return value === "true" || value === "1" ? true : false;
}

export function toNumber(value: string, opts: ToNumberOptions = {}): number | undefined {
  if (value === undefined) {
    return undefined;
  }
  let newValue: number = Number.parseInt(value || String(opts.default), 10);

  if (Number.isNaN(newValue)) {
    newValue = opts.default;
  }

  if (opts.min) {
    if (newValue < opts.min) {
      newValue = opts.min;
    }

    if (newValue > opts.max) {
      newValue = opts.max;
    }
  }

  return newValue;
}

export function toNumberNaN(value: string, opts: ToNumberOptions = {}): number | typeof Number.NaN {
  const result = toNumber(value, opts);
  if (result === undefined) return Number.NaN;
  return result;
}
