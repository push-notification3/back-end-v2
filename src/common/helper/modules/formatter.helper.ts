export function convertUnicodeToNonAccent(str, isLowerCase = true, isReplaceSpace = true) {
  if (!str) {
    return "";
  }
  if (isLowerCase) str = str.toLowerCase();
  if (isReplaceSpace) str = str.replace(/[\s]/g, "-");

  str = str.replace(/[áàảãạăắằẳẵặâấầẩẫậ]/g, "a");
  str = str.replace(/[éèẻẽẹêếềểễệ]/g, "e");
  str = str.replace(/[iíìỉĩị]/g, "i");
  str = str.replace(/[óòỏõọôốồổỗộơớờởỡợ]/g, "o");
  str = str.replace(/[úùủũụưứừửữự]/g, "u");
  str = str.replace(/[ýỳỷỹỵ]/g, "y");
  str = str.replace(/đ/g, "d");

  return str;
}
