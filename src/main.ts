import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { NestExpressApplication } from "@nestjs/platform-express";
import { join } from "path";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors();

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  console.log(join(__dirname, "../../public"));
  app.useStaticAssets(join(__dirname, "../../", "public"), {
    prefix: "/public/",
  });

  app.enableVersioning();

  if (process.env.NODE_ENV === "development") {
    const title = `API Document - ${process.env.NODE_ENV}`;
    const config = new DocumentBuilder()
      .setTitle(title)
      .setDescription("API document")
      .setVersion("1.0")
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup("api-document", app, document);
  }

  await app.listen(process.env.PORT || 3000);

  const appUrl = await app.getUrl();
  console.log("public URL:", `${appUrl}/public`);
  console.log("Application is running on:", appUrl);

  if (process.env.NODE_ENV === "development") console.log("Swagger:", `${appUrl}/api-document`);
}
bootstrap();
