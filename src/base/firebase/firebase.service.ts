import { Injectable, ConflictException, NotFoundException } from "@nestjs/common";
import { CreateFirebaseDto } from "./dto/create-firebase.dto";
import { UpdateFirebaseDto } from "./dto/update-firebase.dto";
import { SendMessageDto } from "./dto/send-message.dto";
import { PrismaService } from "src/prisma/prisma.service";
import { FirebaseAdmin, InjectFirebaseAdmin } from "nestjs-firebase";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";

@Injectable()
export class FirebaseService {
  constructor(
    @InjectFirebaseAdmin() private readonly firebase: FirebaseAdmin,
    private prisma: PrismaService,
  ) {}

  async create(createFirebaseDto: CreateFirebaseDto, userId: number) {
    try {
      const firebase = await this.prisma.firebaseToken.findFirst({
        where: {
          userId,
        },
      });
      if (firebase && firebase.token === createFirebaseDto.token) {
        return firebase;
      }

      const res = await this.prisma.firebaseToken.create({
        data: {
          token: createFirebaseDto.token,
          userId,
        },
      });
      return res;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === "P2002") {
          throw new ConflictException("Token đã tồn tại");
        }
      }
      throw error;
    }
  }

  async update(updateFirebaseDto: UpdateFirebaseDto, userId: number) {
    try {
      const res = await this.prisma.firebaseToken.update({
        where: {
          userId,
        },
        data: {
          token: updateFirebaseDto.token,
        },
      });
      return res;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === "P2002") {
          throw new ConflictException("Token đã tồn tại");
        }
      }
      throw error;
    }
  }

  async findOne(userId: number) {
    try {
      return await this.prisma.firebaseToken.findFirstOrThrow({
        where: {
          userId: userId,
        },
      });
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        // not found
        if (error.code === "P2025") {
          throw new NotFoundException("Token không tồn tại");
        }
      }
      throw error;
    }
  }

  async remove(userId: number) {
    try {
      const firebase = await this.prisma.firebaseToken.findFirstOrThrow({
        where: {
          userId,
        },
      });
      return await this.prisma.firebaseToken.delete({
        where: {
          id: firebase.id,
        },
      });
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        // not found
        if (error.code === "P2025") {
          throw new NotFoundException("Token không tồn tại");
        }
      }
      throw error;
    }
  }

  async sendMessage(sendTo: number, sendMessageDto: SendMessageDto, username: string) {
    try {
      const firebase = await this.prisma.firebaseToken.findFirst({
        where: {
          userId: sendTo,
        },
      });
      console.log(sendTo, sendMessageDto, username, firebase);
      if (!firebase) {
        return {};
      }
      const res = await this.firebase.messaging.send({
        data: {
          icon: "https://picsum.photos/536/354",
          title: `Tin nhắn từ ${username}`,
          body: sendMessageDto.content,
          fromUser: username,
          type: "chat",
        },
        token: firebase.token,
      });
      return res;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        // not found
        if (error.code === "P2025") {
          throw new NotFoundException("Token không tồn tại");
        }
      }
      throw error;
    }
  }

  async joinTopic(topicId: number, userId: number) {
    try {
      const token = await this.prisma.firebaseToken.findFirst({
        where: {
          userId,
        },
      });
      if (!token) {
        throw new NotFoundException("Bạn chưa đăng ký nhận thông báo");
      }
      console.log("token", token);
      // get topic
      const topic = await this.prisma.firebaseTopic.findFirst({
        where: {
          id: topicId,
        },
      });
      // add to topic
      const member = await this.prisma.firebaseTopicMember.create({
        data: {
          topicId: topic.id,
          tokenId: token.id,
        },
      });
      await this.firebase.messaging.subscribeToTopic(token.token, topic.key);

      return member;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        // not found
        if (error.code === "P2025") {
          throw new NotFoundException("Token không tồn tại");
        }
      }
      throw error;
    }
  }

  async leaveTopic(topicId: number, userId: number) {
    try {
      const token = await this.prisma.firebaseToken.findFirst({
        where: {
          userId,
        },
      });
      if (!token) {
        throw new NotFoundException("Bạn chưa đăng ký nhận thông báo");
      }
      // get topic
      const topic = await this.prisma.firebaseTopic.findFirst({
        where: {
          id: topicId,
        },
      });
      // add to topic
      const member = await this.prisma.firebaseTopicMember.deleteMany({
        where: {
          topicId: topic.id,
          tokenId: token.id,
        },
      });
      await this.firebase.messaging.unsubscribeFromTopic(token.token, topic.key);

      return member;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        // not found
        if (error.code === "P2025") {
          throw new NotFoundException("Token không tồn tại");
        }
      }
      throw error;
    }
  }

  async getTopics(userId: number) {
    try {
      const topics = await this.prisma.firebaseTopic.findMany({
        include: {
          FirebaseTopicMember: {
            where: {
              FirebaseToken: {
                userId,
              },
            },
          },
        },
      });
      return topics;
    } catch (error) {
      throw error;
    }
  }

  async sendMessageToTopic(topicId: number, sendMessageDto: SendMessageDto) {
    try {
      const topic = await this.prisma.firebaseTopic.findFirst({
        where: {
          id: topicId,
        },
      });
      if (!topic) {
        throw new NotFoundException("Topic không tồn tại");
      }
      const members = await this.prisma.firebaseTopicMember.findMany({
        where: {
          topicId: topic.id,
        },
      });
      if (!members || members.length === 0) {
        throw new NotFoundException("Không có thành viên trong topic");
      }
      await this.firebase.messaging.send({
        data: {
          icon: "https://picsum.photos/536/354",
          title: `Thông báo từ ${topic.name}`,
          body: sendMessageDto.content,
          type: "topic",
        },
        topic: topic.key,
      });
      return {
        success: true,
      };
    } catch (error) {
      throw error;
    }
  }
}
