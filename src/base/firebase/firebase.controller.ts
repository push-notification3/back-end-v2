import { Controller, Get, Post, Body, Patch, Delete, HttpCode, UseGuards, Param } from "@nestjs/common";
import { FirebaseService } from "./firebase.service";
import { CreateFirebaseDto } from "./dto/create-firebase.dto";
import { UpdateFirebaseDto } from "./dto/update-firebase.dto";
import { SendMessageDto } from "./dto/send-message.dto";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtGuard } from "src/auth/guard";
import { GetUser } from "src/auth/decorator";
import { JwtPayload } from "src/auth/strategy";

@Controller({
  path: "firebase",
  version: "1",
})
@ApiTags("Firebase")
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class FirebaseController {
  constructor(private readonly firebaseService: FirebaseService) {}

  @Post()
  create(@Body() createFirebaseDto: CreateFirebaseDto, @GetUser() user: JwtPayload) {
    return this.firebaseService.create(createFirebaseDto, user.sub);
  }

  @Patch()
  update(@Body() updateFirebaseDto: UpdateFirebaseDto, @GetUser() user: JwtPayload) {
    return this.firebaseService.update(updateFirebaseDto, user.sub);
  }

  @Get()
  findOne(@GetUser() user: JwtPayload) {
    return this.firebaseService.findOne(user.sub);
  }

  @Delete()
  @HttpCode(204)
  remove(@GetUser() user: JwtPayload) {
    return this.firebaseService.remove(user.sub);
  }

  @Post("send-message/:userId")
  sendMessage(
    @Param("userId") userId: string,
    @Body() sendMessageDto: SendMessageDto,
    @GetUser() user: JwtPayload,
  ) {
    return this.firebaseService.sendMessage(+userId, sendMessageDto, user.username);
  }

  @Post("topics/join/:topicId")
  joinTopic(@Param("topicId") topicId: string, @GetUser() user: JwtPayload) {
    return this.firebaseService.joinTopic(+topicId, user.sub);
  }

  @Delete("topics/leave/:topicId")
  leaveTopic(@Param("topicId") topicId: string, @GetUser() user: JwtPayload) {
    return this.firebaseService.leaveTopic(+topicId, user.sub);
  }

  @Get("topics")
  getTopics(@GetUser() user: JwtPayload) {
    return this.firebaseService.getTopics(user.sub);
  }

  @Post("topics/send-message/:topicId")
  sendMessageToTopic(@Param("topicId") topicId: string, @Body() sendMessageDto: SendMessageDto) {
    return this.firebaseService.sendMessageToTopic(+topicId, sendMessageDto);
  }
}
