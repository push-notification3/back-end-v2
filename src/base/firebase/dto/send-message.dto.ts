import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "class-validator";

export class SendMessageDto {
  @ApiProperty({
    required: true,
    description: "content",
  })
  @IsString()
  @IsNotEmpty({ message: "Nội dung không được để trống" })
  content: string;
}
