import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "class-validator";

export class CreateFirebaseDto {
  @ApiProperty({
    required: true,
    description: "token",
  })
  @IsString()
  @IsNotEmpty({ message: "Token không được để trống" })
  token: string;
}
