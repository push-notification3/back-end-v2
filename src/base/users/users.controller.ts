import { Controller, Get, UseGuards } from "@nestjs/common";
import { UsersService } from "./users.service";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtGuard } from "src/auth/guard";
import { JwtPayload } from "src/auth/strategy";
import { GetUser } from "src/auth/decorator";

@Controller({
  path: "users",
  version: "1",
})
@ApiTags("Users")
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  findAll(@GetUser() user: JwtPayload) {
    return this.usersService.findAll(user.sub);
  }
}
